package com.bankingsystem.service.impl;

import com.bankingsystem.entity.User;
import com.bankingsystem.model.LoginModel;
import com.bankingsystem.model.UserDetails;
import com.bankingsystem.repository.impl.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    //private Optional<User> ;

    public ResponseEntity createUser(UserDetails userDetails) throws Exception {
        System.out.println(userDetails);
        Optional<User> user1 = userRepository.findById(userDetails.getUserName());

        System.out.println("optional object " + user1.isEmpty());
        if (!user1.isEmpty()) {
            throw new Exception();
        }
        User user = new User();
        Date date = new Date();
        user.setName(userDetails.getName());
        user.setUserName(userDetails.getUserName());
        String pwd = userDetails.getPassword();
        pwd = Base64.getEncoder().encodeToString(pwd.getBytes());
        user.setPassword(pwd);
        user.setAdharNumber(userDetails.getAdharNumber());
        user.setDateOfBirth(userDetails.getDateOfBirth());
        user.setMobileNumber(userDetails.getMobileNumber());
        user.setEmailId(userDetails.getEmailId());
        user.setUserRole(userDetails.getUserRole());
        user.setCreateDate(date);
        user.setModifiedDate(date);
        user.setIFSCCode("IFSC123456");
        user.setAccountNumber("AC12345678");
        user.setAccountType(userDetails.getAccountType());
        user.setCity(userDetails.getCity());
        user.setBranchCode("Branch1");
        user.setTransactionRights(userDetails.getTransactionRights());
        user.setAccountBalance(0.00);


        System.out.println(userDetails.toString());
        User user2 = userRepository.save(user);
        return ResponseEntity.ok().body(user2);
    }

    public UserDetails updateUser(UserDetails userDetails) throws Exception {
        System.out.println(userDetails);
        Optional<User> user1 = userRepository.findById(userDetails.getUserName());
        System.out.println("optional object in updateUser" + user1.isEmpty());
//        if(!user1.isEmpty()) {
//            throw new Exception();
//        }
        User user = new User();

        user.setName(userDetails.getName());
        user.setUserName(userDetails.getUserName());
        user.setAdharNumber(userDetails.getAdharNumber());
        user.setDateOfBirth(userDetails.getDateOfBirth());
        user.setEmailId(userDetails.getEmailId());
        user.setMobileNumber(userDetails.getMobileNumber());
        user.setPassword(Base64.getEncoder().encodeToString(userDetails.getPassword().getBytes()));
        System.out.println(userDetails.toString());
        User user2 = userRepository.save(user);
        return userDetails;


    }

    public ResponseEntity updateEmail(String userName, String emailId) throws Exception {
        System.out.println("username & emailid" + userName + "" + emailId);
        Optional<User> user1 = userRepository.findById(userName);
        System.out.println("optional object in updateUser" + user1.isEmpty());
        if (user1.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("userName not found");
        }
        Date date = new Date();
        int count = userRepository.UpdateEmail(userName, emailId, date);
        return ResponseEntity.ok().body(count);


    }

    public ResponseEntity updatePassword(String userName, String password, String newPassword) throws Exception {
        Optional<User> user1 = userRepository.findById(userName);
        System.out.println("optional object in updateUser" + user1.isEmpty());
        if (user1.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("userName not found");
        }
        Date date = new Date();
        String pwd = password;
        pwd = Base64.getEncoder().encodeToString(pwd.getBytes());
      //  user.setPassword(pwd);
        String newPwd = newPassword;
        newPwd = Base64.getEncoder().encodeToString(newPwd.getBytes());
        //user.setPassword(newPwd);
        int count = userRepository.UpdatePassword(userName, pwd, newPwd, date);
        return ResponseEntity.ok().body(count);


    }

    public ResponseEntity deleteUser(String userName, String password) {

        System.out.println(userName);
        //why are we assigning the value to user here. Why not UserDetails object
        Optional<User> user1 = userRepository.findById(userName);
        //why the below statement is required
        User user = user1.get();

        if (!password.equals(user.getPassword())) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("userName not found");
        }
        try {
            userRepository.deleteById(userName);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Not able to delete user. Try later");
        }

        return ResponseEntity.ok().body(userName);
    }

    public ResponseEntity userLogin(LoginModel loginModel) {
        Optional<User> user1 = userRepository.findById(loginModel.getUserName());

        System.out.println("optional object " + user1.isEmpty());
        User user = user1.get();
        if (user1.isEmpty() || !user.getPassword().equals(loginModel.getOldPassword())) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Invalid user name or password");
        }
        UserDetails userDetails = new UserDetails();
        userDetails.setName(user.getName());
        userDetails.setUserName(user.getUserName());
        userDetails.setPassword(user.getPassword());
        userDetails.setAdharNumber(user.getAdharNumber());
        userDetails.setDateOfBirth(user.getDateOfBirth());
        userDetails.setMobileNumber(user.getMobileNumber());
        userDetails.setEmailId(user.getEmailId());
        userDetails.setUserRole(user.getUserRole());
        //creation date
        //modified date
        userDetails.setIFSCCode(user.getIFSCCode());
        userDetails.setAccountNumber(user.getAccountNumber());
        userDetails.setAccountType(user.getAccountType());
        userDetails.setCity(user.getCity());
        userDetails.setBranchCode(user.getBranchCode());
        userDetails.setTransactionRights(user.getTransactionRights());
        userDetails.setAccountBalance(user.getAccountBalance());

        return ResponseEntity.ok().body(userDetails);
    }

    public ResponseEntity balanceEnquiry(String username) {

        Optional<User> user1 = userRepository.findById(username);
        if (user1.isEmpty()) {
           ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found");
        }
        User user = user1.get();
//        UserDetails userDetails = new UserDetails();
//        userDetails.setName(user.getName());
//        userDetails.setUserName(user.getUserName());
//        userDetails.setPassword(user.getPassword());
//        userDetails.setAdharNumber(user.getAdharNumber());
//        userDetails.setDateOfBirth(user.getDateOfBirth());
//        userDetails.setMobileNumber(user.getMobileNumber());
//        userDetails.setEmailId(user.getEmailId());
//        userDetails.setUserRole(user.getUserRole());
//        //creation date
//        //modified date
//        userDetails.setIFSCCode(user.getIFSCCode());
//        userDetails.setAccountNumber(user.getAccountNumber());
//        userDetails.setAccountType(user.getAccountType());
//        userDetails.setCity(user.getCity());
//        userDetails.setBranchCode(user.getBranchCode());
//        userDetails.setTransactionRights(user.getTransactionRights());
//        userDetails.setAccountBalance(user.getAccountBalance());

        return ResponseEntity.ok().body(user.getAccountBalance());
    }
}
