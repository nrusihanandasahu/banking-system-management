package com.bankingsystem.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "userdetails")
public class User {
    private String name;
    @Id
    private String userName;
    private String password;
    private String adharNumber;
    private Date   dateOfBirth;
    private String mobileNumber;
    private String emailId;
    private String userRole;
    private Date   createDate;
    private Date   modifiedDate;
    private String IFSCCode;
    private String accountNumber;
    private String accountType;
    private String city;
    private String branchCode;
    private String transactionRights;
    private Double accountBalance;


}
