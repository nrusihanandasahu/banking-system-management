package com.bankingsystem.repository.impl;

import com.bankingsystem.entity.User;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.FluentQuery;
import org.springframework.stereotype.Repository;

import javax.persistence.Entity;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Repository
public interface  UserRepository extends JpaRepository<User, String> {

    @Query(value = "update User set emailId = :emailId, modifiedDate = :date where userName = :userName")
    @Modifying
    @Transactional
    int UpdateEmail(String userName, String emailId, Date date);

    @Query(value = "update User set password = :newPassword, modifiedDate = :date where userName = :userName and password = :password")
    @Modifying
    @Transactional
    int UpdatePassword(String userName, String password,String newPassword, Date date);
}
