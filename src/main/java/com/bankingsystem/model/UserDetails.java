package com.bankingsystem.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDetails {
    @NotEmpty(message = "Enter Name")
    @NotNull(message = "Enter Name")
    private String name;
    @NotEmpty(message = "Enter user Name")
    @NotNull(message = "Enter user Name")
    @Size(min = 8, max = 12)
    private String userName;
    @NotEmpty(message = "Enter password")
    @NotNull(message = "Enter password")
    //@Pattern(regexp = “^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&-+=()])(?=\\S+$).{8, 12}$”)
    private String password;
    @NotEmpty(message = "Enter Adhar Card")
    @NotNull(message = "Enter Adhar Card")
    @Size(min = 12, max = 12)
    private String adharNumber;
    // validation later
    private Date dateOfBirth;
    @NotEmpty(message = "Enter Mobile#")
    @NotNull(message = "Enter Mobile#")
    private String mobileNumber;
    private String emailId;
    private String userRole;
    private String IFSCCode;
    private String accountNumber;
    private String accountType;
    private String city;
    private String branchCode;
    private String transactionRights;
    private Double accountBalance;

}
