package com.bankingsystem.controller;

import com.bankingsystem.model.LoginModel;
import com.bankingsystem.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    UserService userService;

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody LoginModel loginModel) {
        if(loginModel.getOldPassword() == null || loginModel.getOldPassword().trim().isEmpty() ||
                loginModel.getUserName() == null || loginModel.getUserName().trim().isEmpty()) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad Request");
        }
        return userService.userLogin(loginModel);
    }
}
