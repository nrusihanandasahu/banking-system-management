package com.bankingsystem.controller;

import com.bankingsystem.entity.User;
import com.bankingsystem.model.LoginModel;
import com.bankingsystem.model.UserDetails;
import com.bankingsystem.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class UserController {
    @Autowired
    UserService userService;

    @PostMapping("/createuser")
    public ResponseEntity createUser(@RequestBody @Valid UserDetails userDetails) throws Exception {
        System.out.println(userDetails);
//        if (userDetails.getName() == null || userDetails.getName().trim().isEmpty() ||
//                userDetails.getPassword() == null || userDetails.getPassword().trim().isEmpty() ||
//                userDetails.getUserName() == null || userDetails.getUserName().trim().isEmpty() ||
//                userDetails.getAdharNumber() == null || userDetails.getAdharNumber().trim().isEmpty() ||
//                userDetails.getDateOfBirth() == null ||
//                userDetails.getEmailId() == null || userDetails.getEmailId().trim().isEmpty() ||
//                userDetails.getMobileNumber() == null || userDetails.getMobileNumber().trim().isEmpty()
//
//        ) {
//
//            //Add Response Entity code here
//            //  return ResponseEntity
//
//        }

        return userService.createUser(userDetails);


    }

    @GetMapping("/balanceenquiry/{username}")
    public ResponseEntity balanceEnquiry(@PathVariable String username) {
        if (username == null || username.trim().isEmpty()) {
            ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid User");
        }

        return userService.balanceEnquiry(username);
    }

    @PutMapping("/updateuser")
    public UserDetails updateUser(@RequestBody UserDetails userDetails) throws Exception {

        if (userDetails.getName() == null || userDetails.getName().trim().isEmpty() ||
                userDetails.getPassword() == null || userDetails.getPassword().trim().isEmpty() ||
                userDetails.getUserName() == null || userDetails.getUserName().trim().isEmpty() ||
                userDetails.getAdharNumber() == null || userDetails.getAdharNumber().trim().isEmpty() ||
                userDetails.getDateOfBirth() == null ||
                userDetails.getEmailId() == null || userDetails.getEmailId().trim().isEmpty() ||
                userDetails.getMobileNumber() == null || userDetails.getMobileNumber().trim().isEmpty()

        ) {

            throw new Exception();

        }

        return userService.updateUser(userDetails);

    }

    @PutMapping("/updateemail/updatePassword")
    public ResponseEntity updateEmail(@PathVariable String username, @PathVariable String emailid) throws Exception {


        if (username == null || username.trim().isEmpty() ||
                emailid == null || emailid.trim().isEmpty()) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid input parameters");

        }

        return userService.updateEmail(username, emailid);
    }

    @PutMapping("/updatepassword")
    public ResponseEntity updatePassword(@RequestBody LoginModel loginModel) throws Exception {

        if (loginModel.getUserName() == null || loginModel.getUserName().isEmpty() ||
                loginModel.getOldPassword() == null || loginModel.getOldPassword().isEmpty() ||
                loginModel.getNewPassword() == null || loginModel.getNewPassword().isEmpty() ||
                loginModel.getConfirmNewPassword() == null || loginModel.getConfirmNewPassword().isEmpty() ||
                !loginModel.getNewPassword().equals(loginModel.getConfirmNewPassword())) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid input parameters");

        }

        return userService.updatePassword(loginModel.getUserName(), loginModel.getOldPassword(), loginModel.getNewPassword());


    }

    @DeleteMapping("/deleteuser/{userName}/{password}")
    public ResponseEntity deleteUser(@PathVariable String userName, @PathVariable String password) throws Exception {

        if (userName == null || userName.trim().isEmpty() ||
                password == null || password.trim().isEmpty()) {
            throw new Exception();
        }


        return userService.deleteUser(userName, password);

    }


}
